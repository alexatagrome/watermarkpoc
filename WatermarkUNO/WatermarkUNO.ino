
//const int Rx = 7870;  
const int Rx = 7820;  
const long default_TempC=24;
const long open_resistance=-100;
const long short_resistance=-20;
const long short_CB=240;
const long open_CB=255;

#define SUPPLY_PIN  11
#define SUPPLY_VOL  A0

#define RXSIDE_PIN  10
#define RXSIDE_VOL  A1

void setup() {
  Serial.begin(9600); 

  pinMode(SUPPLY_PIN, OUTPUT);
  pinMode(RXSIDE_PIN, OUTPUT);
  digitalWrite(SUPPLY_PIN, LOW);
  digitalWrite(RXSIDE_PIN, LOW);
  
  delay(100);
}

void loop() {

  // read path A
  digitalWrite(SUPPLY_PIN, HIGH);  
  delayMicroseconds(90);
  int SupplyVoltage=analogRead(SUPPLY_VOL); // supply side
  int APathVoltage=analogRead(RXSIDE_VOL); // Rx-7870 side
  digitalWrite(SUPPLY_PIN, LOW);

  // read path B
  digitalWrite(RXSIDE_PIN, HIGH);  
  delayMicroseconds(90);
  int BSupplyVoltage=analogRead(SUPPLY_VOL); // supply side
  int BPathVoltage=analogRead(RXSIDE_VOL); // Rx-7870 side
  digitalWrite(RXSIDE_PIN, LOW);  

  Serial.print("SUPPLY:");
  Serial.print(SupplyVoltage);
  Serial.print(" A:");
  Serial.print(APathVoltage);
  Serial.print(" B:");
  Serial.print(BPathVoltage);
  Serial.print(" BSUP:");
  Serial.println(BSupplyVoltage); //should always be zero
    
  double SensorResistance = (double(SupplyVoltage)*double(Rx)/double(APathVoltage))-double(Rx);
  Serial.println(SensorResistance);

  long TempC=24;
  int Centibars=0;
  if (SensorResistance>550.00) {
    if(SensorResistance>8000.00) {
      Centibars=-2.246-5.239*(SensorResistance/1000.00)*(1+.018*(TempC-24.00))-.06756*(SensorResistance/1000.00)*(SensorResistance/1000.00)*((1.00+0.018*(TempC-24.00))*(1.00+0.018*(TempC-24.00))); 
      Serial.print("Entered WM1 >8000 Loop \n");
    } 
    if (SensorResistance>1000.00) {
      if (SensorResistance<8000) {
        Centibars=(-3.213*(SensorResistance/1000.00)-4.093)/(1-0.009733*(SensorResistance/1000.00)-0.01205*(TempC)) ;
        Serial.print("Entered WM1 >1000 Loop \n");
      }
    }
    if (SensorResistance<1000.00) {
      if (SensorResistance>550.00) {
        Centibars=-20.00*((SensorResistance/1000.00)*(1.00+0.018*(TempC-24.00))-0.55);
        Serial.print("Entered WM1>550 Loop \n");
      }
    }
  }
  if(SensorResistance<550.00) {
    if(SensorResistance>300.00) {
      Centibars=0.00;
      Serial.print("Entered 550<WM1>0 Loop \n");
    }
    if(SensorResistance<300.00) {
      if(SensorResistance>=short_resistance) {   
        Centibars=short_CB; //240 is a fault code for sensor terminal short
        Serial.print("Entered Sensor Short Loop WM1 \n");
      }     
    }
    if(SensorResistance<=open_resistance){
      Centibars=open_CB; //255 is a fault code for open circuit or sensor not present 
      Serial.print("Entered Open or Fault Loop for WM1 \n");
    }
  }

  Serial.print("Centibars:");
  Serial.println(Centibars);

  /*
  //Apply Calibration Correction
  if(calib_check>1) {
    if(WM1_Resistance>300) {
      WM1_CB=WM1_CB-((calib_check-1.00)*WM1_CB);
      Serial.print("Entered -Calib Loop for WM1 \n");
    }
    if(WM2_Resistance>300) {
      WM2_CB=WM2_CB-((calib_check-1.00)*WM2_CB);
      Serial.print("Entered -Calib Loop for WM2 \n");
    }
  }
  if(calib_check<1) {
    if(WM1_Resistance>300) {
      WM1_CB=WM1_CB+((1.00-calib_check)*WM1_CB);
      Serial.print("Entered +Calib Loop for WM1 \n");
    }
    if(WM2_Resistance>300) {
      WM2_CB=WM2_CB+((1.00-calib_check)*WM2_CB);
      Serial.print("Entered +Calib Loop for WM2 \n");
    }   
  }
  */

  
  delay(5000);
}
